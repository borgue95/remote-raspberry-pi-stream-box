#!/bin/bash
stream_pid=$(ps aux | grep stream.sh | grep -v grep | awk '{print $2}')
ffmpeg_pid=$(ps aux | grep ffmpeg | grep -v grep | awk '{print $2}')
[ -n "$ffmpeg_pid" ] && sudo kill -SIGTERM $ffmpeg_pid
[ -n "$stream_pid" ] && sudo kill -SIGKILL $stream_pid
