# Remote Raspberry Pi stream box

This project is a little script for managing a remote Raspberry Pi which
streams to Youtube 24/7. You can use it, customize it, whatever you want; there
is no secred knowledge in this project. 

## Current setup

This project is not meant to be super generic for every case. So, my current
setup is:

* Raspberry Pi 3
* Camera module v.1 for video
* Logitech C922 Pro for audio
* Internet connection with 4Mbit+ outgoing bandwidth

The script requires a file named `streamkeys` to know which Youtube key to use
depending on the input parameters. This file is not in this repository for
privacy and security reasons. Each of the lines of the file looks like this:

    nameofthekey: your-super-secret-key

## Performance

Raspberry Pi 3 does not have a MJPEG hardware decoder, so it cannot handle the
mjpeg Full HD stream comming from the Logitech C922 Pro webcam. This webcam
uses mjpeg stream or raw images, but using raw images limits the framerate at
high resolutions (5 fps for FullHD) or the resolutions at high framerates (480p
at 24 fps). I use it for audio because is the only device I had in hand when
building this which han deliver audio via USB. 

The Raspberry Pi Camera Module (v1), in the other hand, can deliver h264 video
stream at FullHD at 24 fps, and it does that using specific hardware. So no
need to decode and encode the video input in order to send it to Youtube. With
ffmpeg, you can select the input format and the input framerate. To modify
other parameters, take a look at the output of this command: you will get an
extense list of settings you can modify and which values can take: 

    v4l2-ctl -d /dev/videoX --list-ctlrs-menu

There is also an audio synchronization issue, caused by the camera ISP, that in
FullHD and the selected quality parameters, takes a bit longer to process the
input images, so the audio was comming first. I found that delaying the audio
did not work (for me, anyway), so I advanced the video by 0.8 seconds. It is
not the ideal solution, because ffmpeg detects negative timestamps at the
start, but because it is a live stream, does not matter much.

## How to use it

For now, the help is in catalan, but the source code is in english. It is easy
to gess which flag does what. If not, Google Translate is your friend.

The script can be started headless when starting the Raspberry Pi putting this
lines in the file `/etc/rc.local` before the `exit 0` line:

    cd /home/pi/your/path/here
    ./stream.sh -w 120 &

Here I use an special flag, `-w` that will delay the command by 120 seconds. I
had to implement that wait time before streaming to let the router restart in
case of a power outage. 

This script ensures that ffmpeg is allways running; if it dies, after 15
seconds it will restart. To kill the whole thing, just use the `kill.sh`
script. You may need to use `sudo ./kill.sh` instead of `./kill.sh`.

And that's it. Enjoy!
