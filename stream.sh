#!/bin/bash

# defaults
lloc=santissim
res="1920x1080"
bitrate=3400000
framerate=24
advance_video=-0.8
advance_audio=0
wait_on_boot=0
youtube_key=""

usage() {
    cat << EOF
Usage: $0 
    [-l santissim|orgue]
    [-r 1080|720] 
    [-b num] 
    [-f num] 
    [-v dec] 
    [-a dec] 
    [-w num] 
    [-k code]
    [-u ip]

    -l localització
        Dues opcions. santissim o orgue. Per defecte: santissim
    -r resolució
        Dues opcions. 1080 (1920x1080) o 720 (1280x720). Per defecte: 1080
    -b bitrate
        En bits per segon. Per defecte: 3400000
    -f framerate
        En fotogrames per segon. Per defecte: 24
    -v avança el vídeo
        En segons (decimals). Per defecte: -0.8
    -a avança l'audio
        En segons (decimals). Per defecte: 0
    -w espera abans de començar
        En segons. Per defecte: 0
    -k Youtube code
        Si està habilitat, localització i resolució queden inhabilitades
    -u udp_ip
        Si està habilitat, es descarta localització i resolució. S'enviarà
        al port 7001 a la ip 'udp_ip'. 
EOF
    exit 1
}

check_streamkeys_file() {
    if ! [ -f "streamkeys" ]; then
        echo "No streamkeys file found. Avorting"
        exit 1
    fi
}

print_options() {
    cat << EOF
Lloc:           $lloc
Resolució:      ${res}
Bitrate:        $bitrate bps
Framerate:      $framerate fps
Avança vídeo:   $advance_video segons
Avança audio:   $advance_audio segons
Espera abans:   $wait_on_boot segons
EOF
}

wait_on_boot() {
    if [[ "$wait_on_boot" != "0" ]]; then
        echo
        echo Waiting $wait_on_boot seconds...
        sleep $wait_on_boot
    fi
}

set_camera_options() {
    echo
    echo Setting camera parameters
    v4l2-ctl -d /dev/video2 -c white_balance_auto_preset=3
    v4l2-ctl -d /dev/video2 -c sharpness=10
    v4l2-ctl -d /dev/video2 -c saturation=-10
    v4l2-ctl -d /dev/video2 -c video_bitrate_mode=1
    v4l2-ctl -d /dev/video2 -c h264_profile=4
    v4l2-ctl -d /dev/video2 -c video_bitrate=$bitrate
    v4l2-ctl -d /dev/video2 -c iso_sensitivity_auto=0
    v4l2-ctl -d /dev/video2 -c iso_sensitivity=4
    v4l2-ctl -d /dev/video2 -c auto_exposure=0
    v4l2-ctl -d /dev/video2 -c compression_quality=100
    v4l2-ctl -d /dev/video2 -c h264_i_frame_period=18
}

set_youtube_key() {
    case "$lloc" in
        santissim)
            case "$res" in
                1920x1080)youtube_key=$(grep santissim1080 streamkeys | cut -d" " -f2);;
                1280x720) youtube_key=$(grep santissim720 streamkeys | cut -d" " -f2);;
                *);;
            esac
            ;;
        orgue)
            case "$res" in
                1920x1080)youtube_key=$(grep orgue1080 streamkeys | cut -d" " -f2);;
                1280x720) youtube_key=$(grep orgue720 streamkeys | cut -d" " -f2);;
                *);;
            esac
            ;;
        *);;
    esac
}

re_isnumber='^[0-9]+$'
re_isdecimal='^[+-]?[0-9]+([.][0-9]+)?$'
re_isYoutubeKey='^([a-zA-Z0-9]{4}-){3}[a-zA-Z0-9]{4}$'

while getopts "l:r:b:f:v:a:w:k:u:h" o; do
    case "${o}" in
        l)
            lloc=${OPTARG}
            case "$lloc" in
                santissim|orgue);;
                *) usage;;
            esac 
            ;;
        r)
            res=${OPTARG}
            case "$res" in
                1080)res="1920x1080";;
                720)res="1280x720";;
                *) usage;;
            esac
            ;;
        b)
            bitrate=${OPTARG}
            [[ "$bitrate" =~ $re_isnumber ]] || usage
            ;;
        f)
            framerate=${OPTARG}
            [[ "$framerate" =~ $re_isnumber ]] || usage
            ;;
        v)
            advance_video=${OPTARG}
            [[ "$advance_video" =~ $re_isdecimal ]] || usage
            ;;
        a)
            advance_audio=${OPTARG}
            [[ "$advance_audio" =~ $re_isdecimal ]] || usage
            ;;
        w)
            wait_on_boot=${OPTARG}
            [[ "$wait_on_boot" =~ $re_isnumber ]] || usage
            ;;
        k)
            youtube_key=${OPTARG}
            ;;
        u)
            udp_ip=${OPTARG}
            ;;
        h) usage ;;
        *) usage ;;
    esac
done

[ -n "$youtube_key" ] && [ -n "$udp_ip" ] && usage

check_streamkeys_file
print_options
wait_on_boot
set_camera_options
[ -z "$udp_ip" ] && [ -z "$youtube_key" ] && set_youtube_key

ffmpeg_command="ffmpeg -y "
if [[ "$advance_video" != "0" ]]; then
    ffmpeg_command=$ffmpeg_command"-itsoffset "$advance_video" "
fi
ffmpeg_command=$ffmpeg_command"-f v4l2 -input_format h264 "
ffmpeg_command=$ffmpeg_command"-video_size "$res" "
ffmpeg_command=$ffmpeg_command"-framerate "$framerate" "
ffmpeg_command=$ffmpeg_command"-i /dev/video2 "
if [[ "$advance_audio" != "0" ]]; then
    ffmpeg_command=$ffmpeg_command"-itsoffset "$advance_audio" "
fi
ffmpeg_command=$ffmpeg_command"-f alsa -thread_queue_size 2048 -i hw:1 "
ffmpeg_command=$ffmpeg_command"-vcodec copy "
ffmpeg_command=$ffmpeg_command"-acodec libmp3lame -b:a 128k -ar 44100 "
if [ -n "$youtube_key" ]; then
    ffmpeg_command=$ffmpeg_command"-f flv rtmp://a.rtmp.youtube.com/live2/"$youtube_key
else
    #ffmpeg_command=$ffmpeg_command"-f mpegts udp://${udp_ip}:7000?pkt_size=1316"
    #ffmpeg_command=$ffmpeg_command"-f mpegts udp://127.0.0.1:6000?pkt_size=1316"
    ffmpeg_command=$ffmpeg_command"-f mpegts tcp://${udp_ip}:7004"
fi

#srt_command="srt-live-transmit udp://127.0.0.1:6000 srt://${udp_ip}:7000?mode=caller&oheadbw=50&inputbw=875000&tsbpddelay=2500000"
#srt_command="srt-live-transmit -q -s:3000 -r:3000 udp://127.0.0.1:6000 srt://${udp_ip}:7000"

echo
echo FFmpeg command: $ffmpeg_command
echo Starting live...

while [[ 1 ]]; do
	is_on=$(ps aux | grep ffmpeg | grep -v grep)
	if [ -z "$is_on" ]; then
		$ffmpeg_command &
        #[ -n "$udp_ip" ] && $srt_command &
	fi

	sleep 15
done



